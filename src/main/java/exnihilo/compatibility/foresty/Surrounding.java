package exnihilo.compatibility.foresty;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;

import net.minecraft.block.Block;
import net.minecraft.init.Blocks;
import net.minecraft.world.World;

import forestry.api.apiculture.FlowerManager;

public class Surrounding {
	private static final EnumSet<FlowerType> addedFlowerTypes = EnumSet.of(FlowerType.Normal, FlowerType.Nether, FlowerType.End, FlowerType.Jungle, FlowerType.Mushroom, FlowerType.Cactus, FlowerType.Gourd);

	public Map<String, Integer> blocks = new HashMap<String, Integer>();
	public Map<String, Integer> flowers = new HashMap<String, Integer>();
	public int leafCount;

	public String blockAbove;
	
	public void addBlock(World world, int x, int y, int z)
	{
		Block block = world.getBlock(x, y, z);
	    int meta = world.getBlockMetadata(x, y, z);
		
		if (block != null && block.isLeaves(world, x, y, z))
		{
			leafCount++;
		}
		
		String key = block + ":" + meta;
		
		if (blocks.containsKey(key))
		{
			int count = blocks.get(key);
			
			blocks.put(key, count + 1);
		}else
		{
			blocks.put(key, 1);
		}
		
		tryAddFlower(world, x, y, z);
	}
	
	public void setBlockAbove(Block block, int meta)
	{
		this.blockAbove = block + ":" + meta;
	}
	
	public void tryAddFlower(World world, int x, int y, int z)
	{
		for (FlowerType flowerType : addedFlowerTypes)
		{
			if (FlowerManager.flowerRegistry.isAcceptedFlower(flowerType.getForestryKey(), world, x, y, z))
				addFlower(flowerType);
		}
		
		if (world.getBlock(x, y, z) == Blocks.waterlily)
			addFlower(FlowerType.Water);
	}
	
	private void addFlower(FlowerType type)
	{
		String key = type.name();
		
		if (flowers.containsKey(key))
		{
			int count = flowers.get(key);
			
			flowers.put(key, count + 1);
		}else
		{
			flowers.put(key, 1);
		}
	}
	
	public int getFlowerCount(FlowerType type)
	{
		String key = type.name();
		
		switch (type)
		{
		case None:
			return 0;
		
		default:
			if (flowers.containsKey(key))
			{
				return flowers.get(key);
			}
			else
			{
				return 0;
			}
		}
	}
}
