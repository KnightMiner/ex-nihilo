package exnihilo.events;

import java.util.ArrayList;
import java.util.Iterator;

import cpw.mods.fml.common.eventhandler.EventPriority;
import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import exnihilo.items.hammers.IHammer;
import exnihilo.registries.HammerRegistry;
import exnihilo.registries.helpers.Smashable;
import net.minecraft.block.Block;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemTool;
import net.minecraftforge.event.world.BlockEvent;

public class HandlerHammer {

	@SubscribeEvent(priority=EventPriority.LOWEST)
	public void hammer(BlockEvent.HarvestDropsEvent event)
	{
		if (event.world.isRemote)
			return;

		if (event.harvester == null)
			return;

		if (event.isSilkTouching)
			return;
		
		Block block = event.block;
		int meta = event.blockMetadata;
		ArrayList<Smashable> rewards = HammerRegistry.getRewards(block, meta);
		ItemStack held = event.harvester.getHeldItem();
		
		// if we are using a hammer, it has a high enough harvest level, and there are rewards
		if (isHammer(held) && canHarvest(block, meta, held) && rewards != null && rewards.size() > 0 )
		{
			event.drops.clear();
			event.dropChance = 1f;

			int fortune = EnchantmentHelper.getFortuneModifier(event.harvester);
			Iterator<Smashable> it = rewards.iterator();
			while(it.hasNext())
			{
				Smashable reward = it.next();

				if (event.world.rand.nextFloat() <= reward.chance + (reward.luckMultiplier * fortune))
				{
					event.drops.add(new ItemStack(reward.item, 1, reward.meta));
				}

			}
		}

	}

	/**
	 * Determines if an ItemStack is a hammer
	 * @param stack Input stack
	 * @return True if the stack is a hammer
	 */
	public boolean isHammer(ItemStack stack)
	{
		if (stack == null || stack.getItem() == null)
			return false;

		if (stack.getItem() instanceof IHammer)
			return ((IHammer) stack.getItem()).isHammer(stack);

		if (stack.hasTagCompound() && stack.stackTagCompound.getBoolean("Hammered"))
			return true;

		return false;
	}
	
	/**
	 * Checks the harvest level of the hammer against the block
	 * @param block Input block
	 * @param meta Input meta
	 * @param stack Stack contain the hammer
	 * @return True if the block can be harvested with that level
	 */
	public boolean canHarvest(Block block, int meta, ItemStack stack)
	{
		// already checked against null in the earlier method
		int harvestLevel = 0;
		if (stack.getItem() instanceof ItemTool)
			harvestLevel = ((ItemTool)stack.getItem()).func_150913_i().getHarvestLevel();
		
		return block.getHarvestLevel(meta) <= harvestLevel;
	}

}
