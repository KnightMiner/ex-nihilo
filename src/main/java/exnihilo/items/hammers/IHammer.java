package exnihilo.items.hammers;

import net.minecraft.item.ItemStack;

public interface IHammer {
	
	public boolean isHammer(ItemStack stack);

}
