package exnihilo.registries.helpers;

import net.minecraft.block.Block;
import net.minecraft.util.IIcon;
import net.minecraftforge.fluids.Fluid;

public class Meltable {
	public Block block;
	public int meta;
	public float solidVolume;
	public Fluid fluid;
	public float fluidVolume;
	public Block appearance;
	public int appearanceMeta;
	
	public Meltable(Block block, int meta, float solidAmount, Fluid fluid, float fluidAmount, Block appearance, int appearanceMeta)
	{
		this.block = block;
		this.meta = meta;
		this.solidVolume = solidAmount;
		this.fluid = fluid;
		this.fluidVolume = fluidAmount;
		
		this.appearance = appearance;
		this.appearanceMeta = appearanceMeta;
	}
	

	@Deprecated // kept for compatibility with mods that used this constructor directly
	public Meltable(Block block, int meta, float solidAmount, Fluid fluid, float fluidAmount, Block appearance)
	{
		this(block, meta, solidAmount, fluid, fluidAmount, appearance, 0);
	}
	
	public IIcon getIcon()
	{
		return appearance.getIcon(0, appearanceMeta);
	}
}
